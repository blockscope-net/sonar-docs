# SUI SONAR INSTRUCTIONS

## Access

[JOIN THE DISCORD](https://discord.gg/q9SVnnJN)

First of all, to access the Sui Sonar testnet and mainnet alert channels you need the `@validator` role. For that go to the `#rules` channel and react with required 🖥️ emoji to the roles message.

## Features

Sui Sonar consists of 2 main features: alerts and commands.

- Alerts: are messages tagging users/roles announcing events occurring on the user's node, network, epochs, validators, etc.

- Commands: are messages sent to Sui Sonar to retrieve data about the network, epochs, validators, etc.

## Alerts

There are two types of bot alerts based on the source of the information:

- RPC alerts: are based on querying an external RPC, any user can subscribe to their validator address to be notified on alerts about it

- Node metrics alerts: are based on querying each validator node/metrics endpoint, so the node operator needs to whitelist/open the metrics port to the IP of Sui Sonar, in order to fetch metrics from your validator

Both alert types are optional and you can just execute bot commands and/or read the alert messages.

### RPC Alerts

To subscribe to a validator to receive alerts from, you have to execute the !alert command followed by your validator address. For example:

```sh
!alert 0x2123422b5558dfsd5033fdfsd26b0ea864b37b8rdge451agfgdf6284c2dda531
```

To unsubscribe and stop receiving alerts you have to execute the command with disable subcommand

```sh
!alert disable
```

To check your subscription status just execute the command alone

```sh
!alert
```

- Note: if you have registered a validator node, you're user is automatically subscribed to that validator, when you unregister your node also subscription will be disabled.

### Node Metrics Alerts

To receive metrics alerts and display certain data from your validator you need to register it. For this, it's required that the `/metrics` endpoint is accessible by Sui Sonar server, so you need to whitelist the following IP `136.243.102.226` in your firewall rules.

Here is an example on how to do so with ufw:

```sh
sudo ufw allow from 136.243.102.226 proto tcp to any port 9184
```

Once the port is open, you can register your validator node with the register command followed by your validator address, you validator hostname/ip address and the metrics port (will assume port 9184 if no port is specified).

```sh
!register 0x2123422b5558dfsd5033fdfsd26b0ea864b37b8rdge451agfgdf6284c2dda531 http://my.node.fqdn.com
```

To unregister your node and stop collecting and receiving alerts you have to execute the command with disable subcommand

```sh
!register disable
```

To check your register status just execute the command alone

```sh
!register
```

- Note: in the future, metrics data will be available from Sui metrics platform directly, so you won't need to open any port to benefit from metrics alerts.

## Available Commands

Sui Sonar bot can also execute commands on demand to retrieve from the RPC and display aggregated information. We will be adding new ones to keep improving Sui Sonar features.

Let's walk through the available commands:

- !help : displays help message with commands information

- !network : displays network information and stats (epoch, version, staking, TVL, RGP, TPS, etc.)

- !epochs: displays a list of the latest epochs (version, start height, TPS, RGP, etc.)

- !token: displays SUI token market data (price, volume, market cap, supply, etc.)

- !tps: displays current transactions and checkpoints per second of the network (10 seconds meassurement)

- !status: displays information about the validator you're subscribed to or the one specified (metadata, stake, VP, commission, gas price, tally score, node performance, etc.)

- !validators: displays validators lists with specific info on each list

  - General list: in alphabetical order, displays: name, gas price, next epoch gas price, VP, stake, commission, next epoch commission, etc.
  - Address list: in alphabetical order, displays: name and validator address
  - Reference Gas Price list: ordered by voting power, displays: validators gas price and RGP calculation for next epoch
  - Gas list: list of gas prices alerts for epochs count and 20% token price change SLAs
  - Governance list: list of reported validators and list of validators at risk, displays: name, address, stake, VP, score and reporters

## Available Alerts

### ⛓️ Validator alerts

When a change in validator data, stake or governance occurs.

- Metadata: changes in validator metadata: name and description

- Commission: changes in commission

- Gas: changes in gas price

- Stake: changes in stake

- Tally Score: changes in tally score (another validator reported/unreported your validator)

- At Risk: changes in validator at risk epoch count

### ⏳ Epoch alerts

When a new epoch starts the following alerts are sent. (\*requires alerts epoch role).

- Epoch: data for the new epoch

- Gas Price changes: list of validators that haven't changed their gas price (for more than 4 epochs)

- Stake changes: list of validators with their stake change

- Tally Score and At Risk: list of validators reported or at risk

### 🌐 Network alerts

When important events in the network occur an alert is sent. (\*requires alerts network role).

- RPC: is not advancing or is recovering

- TPS: big increases or decreases in transactions per seconds

- Version: package or protocol version change in the RPC

- Safe mode: if the network is in safe mode

### 🪙 Token alerts

When price change events in SUI token occur an alert is sent. (\*requires alerts token role).

- Price: when 5% changes in token price occur

- Epoch: when an epoch hits a 20% change in price between the epoch price and current price

### 🖥️ Node alerts

Alerts based on node metrics service. Only for registered nodes with the metrics port accessible.

- Version: sui-node vesrion change

- Metrics: if node has the metrics service working or is failing

- Peers: network connectivity based on node peers

- Sync: if the node is in sync or not

## Command Cheat Sheet

Several simple commands

```sh
!help        # show commands help (!h)
!cheatsheet  # show commands examples (!cs)
!network     # show network and epoch information (!n)
!epochs      # show list of the last N epochs (!e)
!token       # show token market information (!t)
!tps         # show latest TPS of the network
```

Status command

```sh
!status                            # show status of your validator
!status full                       # show full status of your validator (requires registered node)
!status epochs                     # show status for latest epochs
!status 0xb9b5....758ba            # show status of a validator with address 0xb9b5
!status epochs 0xb9b5....758ba     # show status for latest epochs of a validator with address 0xb9b5
```

Validators command

```sh
!status                   # show status of your validator (!s)
!status full              # show full status of your validator (!s f)
!status epochs            # show status for latest epochs (!s e)
!status 0xb9b5...         # show status of validator with address 0xb9b5  (!s)
!status epochs 0xb9b5...  # show latest epochs status of validator with address 0xb9b5 (!s e)

!validators              # show validators list (!v)
!validators order stake  # show validators list ordered by stake (!v o s)
!validators order fee    # show validators list ordered by fee (!v o f)
!validators order gas    # show validators list ordered by gas price (!v o g)
```

Alert command

```sh
!alert 0xb9b5...  # enable alerts for validator specified by address (!a)
!alert disable    # disable alerts (!a d)
```

Communication channel commands

```sh
!telegram 51...89:AFC...go 23...29                # enable Telegram alerts sent to bot id and chat id (!tg)
!telegram disable                                 # disable alerts (!tg d)

!discord https://discord.com/api/webhooks/234...  # enable Discord alerts sent to webhook (!dd)
!discord disable                                  # disable alerts (!dd d)

!pager ca8...234                                  # enable PagerDuty alerts sent to API integration key (!pd)
!pager disable                                    # disable alerts (!pd d)
```

Register command

```sh
!register 0xb9b5....758ba http://my.domain.com        # register node for validator 0xb9b5 with my.domain.com and default metrics port (9184)
!register 0xb9b5....758ba http://my.domain.com 9999   # register node for validator 0xb9b5 with my.domain.com and 9999 metrics port
!register                                             # show node register status
!register disable                                     # unregister node
```
